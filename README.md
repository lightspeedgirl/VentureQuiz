# VentureQuiz 

VentureQuiz is a quiz app that doesn’t just test your knowledge; in order to master it, you also need a good strategic mind. VentureQuiz uses an API from “Open trivia database” in order to get its questions and a point-based system to reach the players goal as quick as possible. 

The game is made for iOS and mostly coded in Swift.  

## How to play 

The game lengths correlate to the score you need to win the game, so for a short game you need 100 points, medium game you need 200 points, and a hard game you need 300 points. 

The point of the game is to get the max score with the least amount of questions asked. 

The Player is asked to choose the length of the game on the first screen of the game, the player can choose between Short, medium, and long game. 

The player will be allowed to bet his points to get bonus score upon answering the quiz questions so that he can get a bigger score for answering the correct answer. 

When the VentureQuiz starts the player will see a betting screen, they will see the difficulty level of the question, his current score, the question category, and a slider to place their bet. 

## Score 

Score is rewarded to the player upon a correct answer and subtracted upon incorrect answer. Score is multiplied by the difficulty of the question:  

*  Easy questions: reward +(Bet∗1), 
*  Medium questions: reward +(Bet∗2),
*  Hard questions: reward +(Bet∗3).
  
The bet is always placed before the players receive the question and can be between 1 and 10 points. 

## Lifelines 

If you need help you can shake the phone to remove two of the incorrect answers. This brings with it some disadvantages such as only being rewarded half of the points for a correct answer or loosing points for each incorrect answer. 

### Example 

* Correct Medium answer: +(Bet x2)/2
 

* Incorrect Medium answer: −(Bet x2)∗2
 

## Highscore 
Once the player reaches the max score, VentureQuiz ends and the player is shown the amount of questions it took to answer to reach this score. 

## Filenaming
StartScreenViewController.swift
PlayScreenViewController.swift

## ColorNames
gold
darkBlue
darkBlueish
schoolTomato
lightBlueish
 

 