//
//  Extensions.swift
//  VentureQuiz
//
//  Created by Gabriella Stenlund on 2018-11-06.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//
import UIKit

extension UIColor {
    struct ventureColors {
        static let gold = UIColor(named:"gold")
        static let darkBlue = UIColor(named:"darkBlue")
        static let darkBlueish = UIColor(named:"darkBlueish")
        static let schoolTomato = UIColor(named:"schoolTomato")
        static let lightBlueish = UIColor(named:"lightBlueish")
    }
}

extension UIImageView {
    
    // MARK: View Shake Animation
    
    // Shake using preset shaking-values
    func shake1() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.5
        animation.values = [20.0, -20.0, 10.0, -10.0, 5.0, -5.0, 1.0, -1.0, 0.0]
        layer.add(animation, forKey: "shake")
    }
    
    func shake2(view: UIView, for duration: TimeInterval = 0.5, withTranslation translation: CGFloat = 4) {
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.3) {
            view.transform = CGAffineTransform(translationX: translation, y: 0)
        }
        
        propertyAnimator.addAnimations({
            view.transform = CGAffineTransform(translationX: 0, y: 0)
        }, delayFactor: 0.2)
        
        propertyAnimator.startAnimation()
    }
    
    // MARK: View Flash Animation
    // Flash the UIIMageview green, then go back to original color.
    func flashSuccess(view: UIView, for duration: TimeInterval = 0.15, withTranslation translation: CGFloat = 0){
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
        propertyAnimator.addAnimations ({
            view.layer.backgroundColor = UIColor(red: 83/255, green: 244/255, blue: 66/255, alpha: 0.5).cgColor
        }, delayFactor: 0)
        
        propertyAnimator.addCompletion(){_ in
            self.flashSuccessWasFinished(view: view) // go back to original color
        }
        propertyAnimator.startAnimation()
    }
    
    // animated button back to original color
    func flashSuccessWasFinished(view: UIView, for duration: TimeInterval = 0.15, withTranslation translation: CGFloat = 0){
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
        propertyAnimator.addAnimations ({
            view.layer.backgroundColor = UIColor.ventureColors.darkBlue?.cgColor
        }, delayFactor: 0)
        propertyAnimator.startAnimation()
    }
}

extension UIButton {
    
    // MARK: Button Shake Animation
    func shake1() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.5
        animation.values = [20.0, -20.0, 10.0, -10.0, 5.0, -5.0, 1.0, -1.0, 0.0]
        layer.add(animation, forKey: "shake")
    }
    
    // MARK: Button Lifeline Animation
    // Starts the LifelineAnimation. Moves the buttons off the screen with a falling animation.
    // After animation is finished, calls Method to Hide the button.
    func lifelineAnimationHasStarted(view: UIView) {
        let propertyAnimator = UIViewPropertyAnimator(duration: 1, curve: .easeOut, animations: {
            view.transform = CGAffineTransform(translationX: 0, y: 200)
        })
        propertyAnimator.startAnimation()
        
        propertyAnimator.addCompletion() {_ in
            self.lifelineAnimationHasEnded(view: view)
        }
    }

    func lifelineAnimationHasEnded(view: UIView) {
        view.isHidden = true
    }
    
    // MARK: Button Flashing Animation
    // Flashes the button Green. When animation is finishes resets color to Blue.
    func flashRightAnswer(view: UIView, for duration: TimeInterval = 0.3, withTranslation translation: CGFloat = 0){
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
        propertyAnimator.addAnimations ({
            view.layer.backgroundColor = UIColor(red: 83/255, green: 244/255, blue: 66/255, alpha: 0.5).cgColor
        }, delayFactor: 0)
        
        propertyAnimator.addCompletion(){_ in
            self.flashRightAnswerWasFinished(view: view) // go back to original color
        }
        propertyAnimator.startAnimation()
    }
    
    // animated button back to original color
    func flashRightAnswerWasFinished(view: UIView, for duration: TimeInterval = 0.15, withTranslation translation: CGFloat = 0){
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
        propertyAnimator.addAnimations ({
            view.layer.backgroundColor = UIColor.ventureColors.darkBlue?.cgColor
        }, delayFactor: 0)
        propertyAnimator.startAnimation()
    }
    
    // Flashes the button Red to indicate Wrong Answer. After animation has finished resets the color to Blue.
    func flashWrongAnswer(view: UIView, for duration: TimeInterval = 0.3, withTranslation translation: CGFloat = 0){
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
        propertyAnimator.addAnimations ({
            view.layer.backgroundColor = UIColor.ventureColors.schoolTomato?.cgColor
        }, delayFactor: 0)
        
        propertyAnimator.addCompletion(){_ in
            self.flashRightAnswerWasFinished(view: view) // go back to original color
        }
        propertyAnimator.startAnimation()
    }
    
    // animated button back to original color
    func flashWrongAnswerWasFinished(view: UIView, for duration: TimeInterval = 0.15, withTranslation translation: CGFloat = 0){
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
        propertyAnimator.addAnimations ({
            view.layer.backgroundColor = UIColor.ventureColors.darkBlue?.cgColor
        }, delayFactor: 0)
        propertyAnimator.startAnimation()
    }
}




