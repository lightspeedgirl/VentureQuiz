//
//  Enums.swift
//  VentureQuiz
//
//  Created by Jesper Persson on 2018-11-29.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation

public struct HighscoreStruct {
    let username : String
    let questions : Int
    let difficulty : Difficulty
}

enum Difficulty : Int {
    case easy = 30
    case medium = 50
    case hard = 100
}
