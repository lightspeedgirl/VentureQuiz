//
//  OTDBAPIManager.swift
//  VentureQuiz
//
//  Created by Jesper Persson on 2018-11-20.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation

class APIManager{
    //MARK: functions for fetching data from the API
    static public func fetchQuestions(numberOfQuestions: Int, token: String?, completetionHandler: @escaping (Response?, Bool) -> Void){
        if token == nil{
            let urlPath = "https://opentdb.com/api.php?amount=\(numberOfQuestions)&type=multiple&encode=base64"
            retrieveDataFromAPI(url: urlPath, completetionHandler: {responseFromApi, error in
                completetionHandler(responseFromApi, error)
            })
        } else {
            let urlPath = "https://opentdb.com/api.php?amount=\(numberOfQuestions)&encode=base64&token=\(token!)"
            retrieveDataFromAPI(url: urlPath, completetionHandler: {responseFromApi, error in
                completetionHandler(responseFromApi, error)
            })
        }
    }
    
    static public func retrieveSessionToken(completetionHandler: @escaping (Response?, Bool) -> Void){
        let urlPath = "https://opentdb.com/api_token.php?command=request"
        retrieveDataFromAPI(url: urlPath, completetionHandler: {responseFromApi, error in
            completetionHandler(responseFromApi, error)
        })
    }
    
    //MARK: Function for perform the actuall retrieve data from API
    static private func retrieveDataFromAPI(url: String, completetionHandler: @escaping (Response?, Bool) -> Void){
        guard let apiURL = URL(string: url) else {
            completetionHandler(nil, false)
            return
        }
        
        var request = URLRequest(url: apiURL)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                let decoder = JSONDecoder()
                decoder.dataDecodingStrategy = .base64
                let questionData = try? decoder.decode(Response.self, from: data)
                completetionHandler(questionData, true)
            }else {
                completetionHandler(nil, false)
            }
        }
        task.resume()
    }
}

//MARK: API-Response struct

//The response from the Open Trivia Database API can be mapped to a Response
//From the API Documentation https://opentdb.com/api_config.php:
/*
 responseCodes:
 Code 0: Success Returned results successfully.
 Code 1: No Results Could not return results. The API doesn't have enough questions for your query. (Ex. Asking for 50 Questions in a Category that only has 20.)
 Code 2: Invalid Parameter Contains an invalid parameter. Arguements passed in aren't valid. (Ex. Amount = Five)
 Code 3: Token Not Found Session Token does not exist.
 Code 4: Token Empty Session Token has returned all possible questions for the specified query. Resetting the Token is necessary.
 */
struct Response: Codable{
    let responseCode: Int
    var questions: [Question]?
    let token: String?
    
    private enum CodingKeys: String, CodingKey {
        case questions = "results"
        case responseCode = "response_code"
        case token
    }
}
