//
//  HighscoreCell.swift
//  VentureQuiz
//
//  Created by Markus Eriksson on 2018-11-15.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation
import UIKit

class HighscoreCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
