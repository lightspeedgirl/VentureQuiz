//
//  bettingScreenViewController.swift
//  VentureQuiz
//
//  Created by Gabriella Stenlund on 2018-11-06.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import UIKit

class BettingScreenViewController: UIViewController {

    //MARK: - Variables for objects
    var _gameSession: GameSession?
    
    private var _question: Question?
    //MARK: - Variables
    private var _easyImages: [UIImage] = []
    private var _hardImages: [UIImage] = []
    private var _bettingNumber: Int = 2
    
    
    //MARK: - Outlets
    @IBOutlet weak var _themeLabel: UILabel!
    @IBOutlet weak var _pointerImageHolderImageView: UIImageView!
    @IBOutlet weak var _currentPointsLabel: UILabel!
    @IBOutlet weak var _bettingPointsLabel: UILabel!
    @IBOutlet weak var _slider: UISlider!
    
    
    // Mark: Methods
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.items?[0].title = "Quit"
        super.viewWillAppear(animated)
        self._question = _gameSession?.getQuestion()
        
        if self._question?.incorrectAnswers!.count == 0{
            let alert = UIAlertController (title: "Data error", message: "There are no more questions in your device, please activate data and reload the app", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                exit(0)
                
            }))
            self.present(alert, animated: true)
        }
        
        self._themeLabel.text = "Theme: "+(self._question?.category)!
        self._pointerImageHolderImageView.animationDuration = 0.8
        self._pointerImageHolderImageView.animationRepeatCount = 1
        _currentPointsLabel.text = String(_gameSession?.currentPoints ?? 0)+" Points"
        
        
        //MARK: - Animation decider
        if self._question?.difficulty == "easy"{
            self._pointerImageHolderImageView.animationImages = self._easyImages
            self._pointerImageHolderImageView.startAnimating()
            self._pointerImageHolderImageView.image = (UIImage(named: "easyDiffi_00043"))
        }
        else if self._question?.difficulty == "medium"{
            self._pointerImageHolderImageView.image = (UIImage(named: "hardDiffi_00000"))
        }
        else if self._question?.difficulty == "hard"{
            self._pointerImageHolderImageView.animationImages = self._hardImages
            self._pointerImageHolderImageView.startAnimating()
            self._pointerImageHolderImageView.image = (UIImage(named: "hardDiffi_00043"))
            
        }
    }
    
    //MARK: - Animation
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in stride(from: 0, through: 43, by: 1) {
            if i < 10{
                self._easyImages.append(UIImage(named: "easyDiffi_0000"+String(i))!)
                self._hardImages.append(UIImage(named: "hardDiffi_0000"+String(i))!)
            }
            else{
                self._easyImages.append(UIImage(named: "easyDiffi_000"+String(i))!)
                self._hardImages.append(UIImage(named: "hardDiffi_000"+String(i))!)
            }
        }
        self.tabBarController?.tabBar.items?[0].title = "Quit"
    }
    //MARK: - Slider
    @IBAction func bettingSliderChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)*2
        
        self._bettingNumber = Int(sender.value)*2
        _bettingPointsLabel.text = "Bet: \(currentValue)"
    }
    
    //MARK: - prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PlayScreenViewController {
            destination._question = _question
            self._gameSession?.addBet(pointsToBet: self._bettingNumber)
            let GS = self._gameSession
            destination._gameSession = GS
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

