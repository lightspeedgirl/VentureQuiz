//
//  WinScreenViewController.swift
//  VentureQuiz
//
//  Created by Gabriella Stenlund on 2018-11-06.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import UIKit

class WinScreenViewController: UIViewController{

    
    // MARK: Outlets
    @IBOutlet weak var _playAgainButton: RoundedCornerButton!
    @IBOutlet weak var _playerNameField: UITextField!
    @IBOutlet weak var _saveScoreButton: RoundedCornerButton!
    
    
    // MARK: Variables
    private var _score: Int = 0
    private var _difficulty: Int = 0
    private var _questions: Int = 0
    var _gameSession: GameSession?
    
    // MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        _difficulty = _gameSession?.pointsToReach ?? 0
        _questions = _gameSession?.getAmountOfQuestionsAnswered() ?? 0
        _score = _gameSession?.currentPoints ?? 0
   
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(WinScreenViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.tabBarController?.tabBar.items?[0].title = "Play"
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        if _playerNameField.isEditing == true {
            view.endEditing(true)
        }
    }

    
    func saveCurrentScoreToDB(_ name: String, _ questions: Int, _ difficulty: Int) {
        
        // Saves the highscore inforation to db.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let highScoreDb = HighScoreDBManager(appDelegate: appDelegate)
        var theDifficulty = Difficulty.easy
        if difficulty == Difficulty.medium.rawValue {theDifficulty = Difficulty.medium}
        if difficulty == Difficulty.hard.rawValue {theDifficulty = Difficulty.hard}
        
        highScoreDb.saveHighScore(newScore: HighscoreStruct(username: name, questions: questions, difficulty: theDifficulty))
        
    }
   
    
    // MARK: Buttons
    @IBAction func buttonToPlay(_ sender: Any) {
        performSegue(withIdentifier: "unwindWinScreen", sender: self)
    }
    
    // If the player has entered a name, send the name and relevant info to SaveCurrentScoreToDB
    // Disables the button and changes text color.
    @IBAction func saveHighscoreButton(_ sender: Any) {
        if (_playerNameField!.text != nil) {
            saveCurrentScoreToDB(_playerNameField.text!, _questions, _difficulty)
            self._saveScoreButton.isEnabled = false
            self._saveScoreButton.setTitleColor(UIColor.darkGray, for: .normal)
            self._playerNameField.text = ""
        } else {
            print("No name filled in")
        }
    }
}
