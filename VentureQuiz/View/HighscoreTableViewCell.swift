//
//  HighscoreTableViewCell.swift
//  VentureQuiz
//
//  Created by Markus Eriksson on 2018-11-16.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import UIKit

class HighscoreTableViewCell: UITableViewCell {

    @IBOutlet weak var questionAmountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var difficultyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
