//
//  GameOverScreenViewController.swift
//  VentureQuiz
//
//  Created by Gabriella Stenlund on 2018-11-06.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import UIKit

class GameOverScreenViewController: UIViewController {

    // MARK: Outlets
   // @IBOutlet weak var _replayView: UIView!
    
    
    
    // MARK: Variables
    
    // MARK: Methods
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.items?[0].title = "Play"
        // Do any additional setup after loading the view.
    }

    // MARK: Buttons
    @IBAction func toPlayButton(_ sender: Any) {
        performSegue(withIdentifier: "unwind", sender: self)
    }
}
