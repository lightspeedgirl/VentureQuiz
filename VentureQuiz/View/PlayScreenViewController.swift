//
//  PlayScreenViewController.swift
//  VentureQuiz
//
//  Created by Gabriella Stenlund on 2018-11-06.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import UIKit

class PlayScreenViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var _button1: RoundedCornerButton!
    @IBOutlet weak var _button2: RoundedCornerButton!
    @IBOutlet weak var _button3: RoundedCornerButton!
    @IBOutlet weak var _button4: RoundedCornerButton!
    @IBOutlet weak var _questionPlacerLabel: UILabel!
    @IBOutlet weak var _ventureQuestionView: UIView!
    @IBOutlet weak var _pointsLabel: UILabel!
    @IBOutlet weak var _lostPointsLabel: UILabel!
    @IBOutlet weak var _winPointsLabel: UILabel!
    // MARK: Buttons
    
    
    // MARK: Variables
    var _gameSession: GameSession?
    var _question: Question?
    
    
    // MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.ventureColors.darkBlueish
        _ventureQuestionView.layer.cornerRadius = 10
        self.puttingOutQuestion()
        self.becomeFirstResponder()
        let potentialPoints = self._gameSession?.getPotentialPoints()
        _lostPointsLabel.text = String(potentialPoints!.loose)
        _winPointsLabel.text = String(potentialPoints!.win)
        // Do any additional setup after loading the view.
    }
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    func puttingOutQuestion(){
        //TODO: Handle Boolean Question type
        //var question = self.qg.currentQuestion
        _button2.setTitle(_gameSession?.getButtonText(1), for: .normal)
        _button2.accessibilityIdentifier = String(1)
        _button2.addTarget(self, action:#selector(buttonAction), for: .touchUpInside)
        
        _button1.setTitle(_gameSession?.getButtonText(0), for: .normal)
        _button1.accessibilityIdentifier = String(0)
        _button1.addTarget(self, action:#selector(buttonAction), for: .touchUpInside)
        _button3.setTitle(_gameSession?.getButtonText(2), for: .normal)
        _button3.accessibilityIdentifier = String(2)
        _button3.addTarget(self, action:#selector(buttonAction), for: .touchUpInside)
        
        _button4.setTitle(_gameSession?.getButtonText(3), for: .normal)
        _button4.accessibilityIdentifier = String(3)
        _button4.addTarget(self, action:#selector(buttonAction), for: .touchUpInside)
    
        _questionPlacerLabel.text = "" + (_question?.question)!
        _pointsLabel.text = String(_gameSession?.currentPoints ?? 10)+"/"+String(_gameSession?.pointsToReach ?? 0)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? BettingScreenViewController {
            let GS = self._gameSession
            destination._gameSession = GS
        }
        if let destination = segue.destination as? WinScreenViewController{
            let GS = self._gameSession
            destination._gameSession = GS
        }
    }
    
    // Device Shaked listener. If device was shaken initiate animation and removal of 2 incorrect answers.
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            self._gameSession?.hasShaken = true
            let potentialPoints = self._gameSession?.getPotentialPoints()
            _lostPointsLabel.text = String(potentialPoints!.loose)
            _winPointsLabel.text = String(potentialPoints!.win)
            let removed = _gameSession?.removed
            
            if(removed![0] == 0){
                _button1.setTitle("", for: .normal)
                _button1.lifelineAnimationHasStarted(view: _button1)
            } else if(removed![0] == 1){
                _button2.setTitle("", for: .normal)
                _button2.lifelineAnimationHasStarted(view: _button2)
            } else if(removed![0] == 2){
                _button3.setTitle("", for: .normal)
                _button3.lifelineAnimationHasStarted(view: _button3)
            } else if(removed![0] == 3){
                _button4.setTitle("", for: .normal)
                _button4.lifelineAnimationHasStarted(view: _button4)
            }
            
            if(removed![1] == 0){
                _button1.setTitle("", for: .normal)
                _button1.lifelineAnimationHasStarted(view: _button1)
            } else if(removed![1] == 1){
                _button2.setTitle("", for: .normal)
                _button2.lifelineAnimationHasStarted(view: _button2)
            } else if(removed![1] == 2){
                _button3.setTitle("", for: .normal)
                _button3.lifelineAnimationHasStarted(view: _button3)
            } else if(removed![1] == 3){
                _button4.setTitle("", for: .normal)
                _button4.lifelineAnimationHasStarted(view: _button4)
            }
            
        }
    }
    
    
    // MARK: Buttons
    // ButtonAction handles the four answer buttons on the screen.
    // Check if the answer is Right or Wrong, plays the correct animation accordingly. Segues to Win, Play, or Loose screen.
    @objc func buttonAction(sender: RoundedCornerButton){
        let isRight = _gameSession!.clickedRightorWrongAnswer(answer: Int(sender.accessibilityIdentifier!)!)
        
        _gameSession!.calulatePoints(answer: Int(sender.accessibilityIdentifier ?? "3")!)
        let won = _gameSession?.hasWon()
        let lost = _gameSession?.hasLost()
        if lost! && !won! {
            sender.flashWrongAnswer(view: sender)
            DispatchQueue.main.asyncAfter(deadline:.now() + 0.5, execute: {
            print("Game Over")
            self.performSegue(withIdentifier: "toGameOverSegue", sender: self)
            })
        }
        else if !lost! && !won! {
            if isRight {
                sender.flashRightAnswer(view: sender)
            } else {
                sender.flashWrongAnswer(view: sender)
            }
            DispatchQueue.main.asyncAfter(deadline:.now() + 0.5, execute: {
                self.navigationController?.popViewController(animated: false)
            })
        }
        else if !lost! && won! {
            sender.flashRightAnswer(view: sender)
            DispatchQueue.main.asyncAfter(deadline:.now() + 0.5, execute: {
                self.performSegue(withIdentifier: "toWinSegue", sender: self)
                print("win")
            })
        }
    }
}
