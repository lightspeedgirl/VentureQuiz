//
//  HighScoreScreenTableViewController.swift
//  VentureQuiz
//
//  Created by Gabriella Stenlund on 2018-11-06.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import UIKit

class HighScoreScreenTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: Outlets
    @IBOutlet weak var _highscoreTableView: UITableView!
    @IBOutlet weak var _currentCathegoryLabel: UILabel!
    
   
    
    
    // MARK: Variables
    var _currentDifficulty: Difficulty = Difficulty.easy
    var _scores: [HighscoreStruct] = []
   
    
    
    // MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        _highscoreTableView.delegate = self
        _highscoreTableView.dataSource = self
        self.tabBarController?.tabBar.items?[0].title = "Play"
        
        swapHighscoreBoard()
    }
    
    // Swaps the information in the HighscoreTable
    // Fetches the score list from database, and sets the tite, and then reloads the table.
    func swapHighscoreBoard(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let highScoreDb = HighScoreDBManager(appDelegate: appDelegate)
        _scores = highScoreDb.getHighScore(difficulty: _currentDifficulty)
       
        
        // swap between difficulty
        switch _currentDifficulty {
        case Difficulty.easy:
            _currentCathegoryLabel.text = "\(Difficulty.easy.rawValue)"
        case Difficulty.medium:
            _currentCathegoryLabel.text = "\(Difficulty.medium.rawValue)"
        case Difficulty.hard:
            _currentCathegoryLabel.text = "\(Difficulty.hard.rawValue)"
        }
        _highscoreTableView.reloadData()
    }
    
    
    
    
    
    // MARK: Buttons
    // Increments the difficulty / score cathegories. Calls to swap and reload the table.
    @IBAction func incrementRightButton(_ sender: Any) {
        switch _currentDifficulty {
        case Difficulty.easy:
            _currentDifficulty = Difficulty.medium
        case Difficulty.medium:
            _currentDifficulty = Difficulty.hard
        case Difficulty.hard:
            _currentDifficulty = Difficulty.easy
        }
        swapHighscoreBoard()
    }
    
    @IBAction func incrementLeftButton(_ sender: Any) {
        switch _currentDifficulty {
        case Difficulty.easy:
            _currentDifficulty = Difficulty.hard
        case Difficulty.medium:
            _currentDifficulty = Difficulty.easy
        case Difficulty.hard:
            _currentDifficulty = Difficulty.medium
        }
        swapHighscoreBoard()
    }
    
    
    
    
    
    
    // MARK: Table Functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _scores.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "highscoreCell", for: indexPath) as?
            HighscoreTableViewCell {
            cell.questionAmountLabel.text = "\(_scores[indexPath.row].questions)"
            cell.nameLabel.text = _scores[indexPath.row].username
            cell.difficultyLabel.text = "\(_scores[indexPath.row].difficulty)"
            return(cell)
           
            }
        
        return UITableViewCell()
    }
}


