//
//  StartScreenViewController.swift
//  VentureQuiz
//
//  Created by Gabriella Stenlund on 2018-11-06.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import UIKit
import UserNotifications

class StartScreenViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var _startGameButton: RoundedCornerButton!
    @IBOutlet weak var _difficultyNumberLabel: UILabel!
    @IBOutlet weak var _difficultyStepper: UIStepper!
    
    // MARK: Variables
    var _gameSession : GameSession?
    var _pointsToReach : Int = Difficulty.medium.rawValue
    var _currentDifficulty : Difficulty = Difficulty.medium
    
    
    // MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.ventureColors.darkBlueish
        _startGameButton.backgroundColor = UIColor.ventureColors.darkBlue
        _startGameButton.tintColor = UIColor.ventureColors.gold
        navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        _gameSession = GameSession(appDelegate: appDelegate)
        _difficultyNumberLabel.text = "\(self._currentDifficulty.rawValue)"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Come back and play!"
        notificationContent.body = "Long time no see!"
        notificationContent.sound = UNNotificationSound.default
        
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        let notificationRequest = UNNotificationRequest(identifier: "comeBackNotification", content: notificationContent, trigger: notificationTrigger)
        
        UNUserNotificationCenter.current().add(notificationRequest, withCompletionHandler: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.items?[0].title = "Play"
        _gameSession?.resettGame()
    }
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {
        _gameSession?.resettGame()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? BettingScreenViewController {
            self._gameSession?.setPointToReach(score: _pointsToReach)
            let GS = self._gameSession
            destination._gameSession = GS
        }
    }
    
    
    // MARK: Buttons
    @IBAction func changedDifficulty(_ sender: UIStepper) {
        
    }
    
    
    @IBAction func difficultyStepper(_ sender: UIStepper) {
        switch Int(sender.value) {
        case 0:
            self._currentDifficulty = Difficulty.easy
        case 1:
            self._currentDifficulty = Difficulty.medium
        case 2:
            self._currentDifficulty = Difficulty.hard
        default:
            print("Invalid number, this is not possible")
        }
        _difficultyNumberLabel.text = "\(self._currentDifficulty.rawValue)"
        switch self._currentDifficulty {
        case Difficulty.easy:
            _difficultyNumberLabel.textColor = UIColor.ventureColors.lightBlueish
        case Difficulty.medium:
            _difficultyNumberLabel.textColor = UIColor.ventureColors.gold
        case Difficulty.hard:
            _difficultyNumberLabel.textColor
                = UIColor.ventureColors.schoolTomato
        }
        _pointsToReach = self._currentDifficulty.rawValue
    }
    

}
