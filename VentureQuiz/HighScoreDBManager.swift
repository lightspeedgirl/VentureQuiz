//
//  SQLiteHighScoreManager.swift
//  VentureQuiz
//
//  Created by Jesper Persson on 2018-11-20.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation
import CoreData



class HighScoreDBManager {
    
    // MARK: Variables
    private let _appDelegate : AppDelegate
    private let _context : NSManagedObjectContext
    
    init (appDelegate : AppDelegate) {
        self._appDelegate = appDelegate
        self._context = appDelegate.persistentContainer.viewContext
    }
    
    func saveHighScore (newScore : HighscoreStruct) -> Bool{
        
        var theDifficulty : Int = 0
        switch newScore.difficulty {
        case Difficulty.easy:
            theDifficulty = 0
        case Difficulty.medium:
            theDifficulty = 1
        case Difficulty.hard:
            theDifficulty = 2
        }
        let entity = NSEntityDescription.entity(forEntityName: "Highscore", in: _context)
        let newHighscore = NSManagedObject(entity: entity!, insertInto: _context)
        
        newHighscore.setValue(newScore.username, forKey: "username")
        newHighscore.setValue(newScore.questions, forKey: "questions")
        newHighscore.setValue(theDifficulty, forKey: "difficulty")
        
        do {
            try _context.save()
            self.removeIfToMany(theDifficulty)
            return true
        } catch {
            return false
        }
    }
    
    private func removeIfToMany(_ theDifficulty : Int){
        let highScoreFetch  = NSFetchRequest<Highscore>(entityName: "Highscore")
        highScoreFetch.predicate = NSPredicate(format: "difficulty = %@", "\(theDifficulty)")
        highScoreFetch.sortDescriptors = [NSSortDescriptor(key: #keyPath(Highscore.questions), ascending: true)]
        highScoreFetch.returnsObjectsAsFaults = false
        
        do{
            let fethedHighScore = try _context.fetch(highScoreFetch)
            if(fethedHighScore.count <= 10) { return }
            for i in 10..<fethedHighScore.count {
                try _context.delete(fethedHighScore[i])
            }
        } catch {}
    }
    
    func getHighScore(difficulty : Difficulty) -> [HighscoreStruct] {
        var theDifficulty : Int = 0
        switch difficulty {
        case Difficulty.easy:
            theDifficulty = 0
        case Difficulty.medium:
            theDifficulty = 1
        case Difficulty.hard:
            theDifficulty = 2
        }
        
        
        let highScoreFetch  = NSFetchRequest<Highscore>(entityName: "Highscore")
        highScoreFetch.predicate = NSPredicate(format: "difficulty = %@", "\(theDifficulty)")
        highScoreFetch.sortDescriptors = [NSSortDescriptor(key: #keyPath(Highscore.questions), ascending: true)]
        highScoreFetch.returnsObjectsAsFaults = false
        //highScoreFetch.fetchLimit = 10
        var highScores : [HighscoreStruct] = []
        do{
            let fethedHighScore = try _context.fetch(highScoreFetch)
            for data in fethedHighScore {
                var theDifficulty = Difficulty.easy
                switch(data.value(forKey: "difficulty") as! Int){
                case 1:
                    theDifficulty = Difficulty.medium
                case 2:
                    theDifficulty = Difficulty.hard
                default:
                    theDifficulty = Difficulty.easy
                }
                
                let highScorePost = HighscoreStruct (
                    username: data.value(forKey: "username") as! String,
                    questions: data.value(forKey: "questions") as! Int,
                    difficulty: theDifficulty
                )
                highScores.append(highScorePost)
            }
            return highScores;
        } catch {
            return []
        }
    }
    
}
