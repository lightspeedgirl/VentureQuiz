//
//  Question.swift
//  VentureQuiz
//
//  Created by Måns Sandberg on 2018-11-11.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation


//The Questions-struct
//THe data fetched from the database are mapped into the properties of the struct
//The properties are stored as Data
//To access the properties as Strings use the corresponding getters
//IncorrectAnswers are returned as [String]

class Question: Codable {
    private let _categoryData: String
    private let _typeData: String
    private let _difficultyData: String
    private let _questionData: String
    private let _correctAnswerData: String
    private let _incorrectAnswersData: [String]
    
    init(category : String, type:String, difficulty : String, question: String, correctAnswer : String, incorrectAnswers :[String]){
        self._categoryData = category
        self._typeData = type
        self._difficultyData = difficulty
        self._questionData = question
        self._correctAnswerData = correctAnswer
        self._incorrectAnswersData = incorrectAnswers
        
    }
    
    private enum CodingKeys: String, CodingKey {
        case _categoryData = "category"
        case _difficultyData = "difficulty"
        case _questionData = "question"
        case _typeData = "type"
        case _correctAnswerData = "correct_answer"
        case _incorrectAnswersData = "incorrect_answers"
    }
    
    //Getters for returning Data as String(s)
    var category: String? {
        get{
            return _categoryData
        }
    }
    var type: String? {
        get{
            return _typeData
        }
    }
    var difficulty: String? {
        get{
            return _difficultyData
        }
    }
    var question: String? {
        get{
            return _questionData
        }
    }
    var correctAnswer: String? {
        get{
            return _correctAnswerData
        }
    }
    
    var incorrectAnswers: [String]? {
        get{
            
            return _incorrectAnswersData
        }
    }
}

extension Question: CustomStringConvertible{
    var description: String{
        return ("Category: \(category!)\nType: \(type!)\nDifficulty: \(difficulty!)\nQuestion: \(question!)\nCorrect Answer: \(correctAnswer!)\nIncorrect answers: \(incorrectAnswers!)\n")
    }
}
