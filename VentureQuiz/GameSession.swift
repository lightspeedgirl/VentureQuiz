//
//  GameSession.swift
//  VentureQuiz
//
//  Created by Jesper Persson on 2018-11-20.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation



class GameSession {
    
    // MARK: Variables
    private var _pointsToReach : Int
    private var _currentPoints : Int
    private var _questionsAnswered : Int
    private var _bet : Int
    private var _hasShaken : Bool
    private var _APISessionToken : String
    private let _questionDBManager : QuestionDBManager
    private var _currentQuestion : Question
    private var _correctIndex : Int
    private var _removed : [Int]
    
    // MARK: Constructor
    init(appDelegate: AppDelegate) {
        self._pointsToReach = 0
        self._currentPoints = 10
        self._questionsAnswered = 0
        self._APISessionToken = ""
        self._bet = 0
        self._correctIndex = 0
        self._hasShaken = false
        self._currentQuestion = Question(category: "", type: "", difficulty: "", question: "", correctAnswer: "", incorrectAnswers: [])
        _questionDBManager = QuestionDBManager(appDelegate: appDelegate)
        _questionDBManager.updateQuestionsIfNeeded()
        self._removed = [ -1, -1]
    }
    
    // MARK: Getters and setters
    public var pointsToReach : Int {
        get {
            return self._pointsToReach
        } set {
            self._pointsToReach = newValue
        }
    }
    
    public var currentPoints : Int {
        get {
            return self._currentPoints
        } set {
            self._currentPoints = newValue
        }
    }
    
    public var questionsAnswered : Int {
        get {
            return self._questionsAnswered
        } set {
            self._questionsAnswered = newValue
        }
    }
    
    public var APISessionToken : String {
        get {
            return self._APISessionToken
        } set {
            self._APISessionToken = newValue
        }
    }
    
    public var hasShaken : Bool {
        get {
            return self._hasShaken
        } set {
            if !self._hasShaken{
                self._hasShaken = newValue
                if self._hasShaken {
                    repeat{
                        self._removed[0] = Int.random(in: 0..<4)
                    } while self._removed[0] == self._correctIndex
                    repeat {
                        self._removed[1] = Int.random(in: 0..<4)
                    } while self._removed[1] == self._removed[0] || self._removed[1] == self._correctIndex
                }
            }
        }
    }
    
    public var removed : [Int] {
        get{
            return self._removed
        }
    }
    
    public var bet : Int {
        get{
            return self._bet
        }
        set {
            self._bet = newValue
        }
    }
    
    public var getLooseScore : Int {
        get {
            if self._hasShaken{
                return -self._bet * 2
            }
            return -self._bet
        }
    }
    
    public var getWinScore : Int {
        get {
            if self._hasShaken{
                return self._bet / 2
            }
            return self._bet
        }
    }
    
    // MARK: Methods
    public func changeCurrentScore(scoreDiff: Int){
        self._currentPoints += scoreDiff
    }
    
    public func getAmountOfQuestionsAnswered()-> Int{
        return self._questionsAnswered
    }
    
    public func increaseAmountOfQuestionsAnswered(){
        self._questionsAnswered += 1
    }
    
    public func setPointToReach(score: Int){
        self._pointsToReach = score
    }
    
    public func getPotentialPoints()-> (win: Int, loose: Int){
        return (self.getWinScore, self.getLooseScore)
    }
    
    public func addBet(pointsToBet: Int){
        self._bet = pointsToBet
    }
    
    public func hasWon()-> Bool{
        return self._currentPoints >= self._pointsToReach
    }
    
    public func hasLost()-> Bool{
        return self._currentPoints <= 0
    }
    
    func isTheAnswerRight(_ index : Int) -> Bool {
        return index == self._correctIndex
    }
    
    func getButtonText(_ index : Int) -> String {
        if index == self._correctIndex {
            return self._currentQuestion.correctAnswer!
        }
        else if index > self._correctIndex{
            return self._currentQuestion.incorrectAnswers![index - 1]
        }
        return self._currentQuestion.incorrectAnswers![index]
    }
    
    public func calulatePoints(answer: Int){

        let rightOrWrong = isTheAnswerRight(answer)
        print("bet")
        print(self._bet)
        var points = 0
        if rightOrWrong{
            points += self.getWinScore
            print(self.getWinScore)
        }
        else{
            points += self.getLooseScore
            print(self.getLooseScore)
        }
        self.increaseAmountOfQuestionsAnswered()
        print("points: ")
        print(points)
        self.changeCurrentScore(scoreDiff: points)
        if(!self.generateQuestion()) {
            self._currentQuestion = Question(category: "", type: "", difficulty: "", question: "", correctAnswer: "", incorrectAnswers: [])
        }
        
    }
    
    public func clickedRightorWrongAnswer(answer: Int) -> Bool {
        let rightOrWrong = isTheAnswerRight(answer)
        return rightOrWrong
    }
    
    
    private func generateQuestion() -> Bool{
        if let newQuestion = self._questionDBManager.getQuestion(){
            self._currentQuestion = newQuestion
            self._correctIndex = Int.random(in: 0..<4)
            self._hasShaken = false
            return true
        }
        return false

    }
    public func getQuestion()-> Question{
        if self._currentQuestion.incorrectAnswers?.count == 0 {
            if(!self.generateQuestion()){
                self._currentQuestion = Question(category: "", type: "", difficulty: "", question: "", correctAnswer: "", incorrectAnswers: [])
            }
        }
        return self._currentQuestion
    }
    public func getQuestionDifficulty()-> String{
        return self._currentQuestion.difficulty ?? "easy"
        
    }
    public func resettGame(){
        self._pointsToReach = 0
        self._currentPoints = 10
        self._questionsAnswered = 0
        self._APISessionToken = ""
        self._bet = 0
        self._correctIndex = 0
        self._hasShaken = false
        self._currentQuestion = Question(category: "", type: "", difficulty: "", question: "", correctAnswer: "", incorrectAnswers: [])
        _questionDBManager.updateQuestionsIfNeeded()
        self._removed = [ -1, -1]
    }
}
