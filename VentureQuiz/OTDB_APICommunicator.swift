//
//  OTDB_APICommunicator.swift
//  VentureQuiz
//
//  Created by Måns Sandberg on 2018-11-15.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation

//TODO: Remove this file


    func fetchQuestions(numberOfQuestions: Int, completetionHandler: @escaping (Response?, Error?) -> Void){
        let urlPath = "https://opentdb.com/api.php?amount=\(numberOfQuestions)&encode=base64"
        let apiURL = URL(string: urlPath)!

        URLSession.shared.dataTask(with: apiURL) { (data, response, error) in
            guard let data = data else {return}
            do{
                let decoder = JSONDecoder()
                decoder.dataDecodingStrategy = .base64
                let questionData = try decoder.decode(Response.self, from: data)
                completetionHandler(questionData, nil)
            }catch let err{
                completetionHandler(nil, err)
            }
            }.resume()
    }
