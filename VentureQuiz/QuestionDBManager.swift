//
//  SQLiteQuestionManager.swift
//  VentureQuiz
//
//  Created by Jesper Persson on 2018-11-20.
//  Copyright © 2018 Måns Sandberg. All rights reserved.
//

import Foundation
import CoreData

class QuestionDBManager {
    // MARK: Variables
    private let _appDelegate : AppDelegate
    private let _context : NSManagedObjectContext
    private let _minStoreLimit = 10
    private let _storeAmount = 15
    
    // MARK: Constructor
    init (appDelegate : AppDelegate) {
        self._appDelegate = appDelegate
        self._context = appDelegate.persistentContainer.viewContext
    }
    
    // MARK: Methods
    
    /* Calculates the amount of questions in the CoreData */
    func questionAmount() -> Int {
        let questionFetch  = NSFetchRequest<QuestionDB>(entityName: "QuestionDB")
        do {
            let amount = try _context.count(for: questionFetch)
            return amount
        } catch {
            print("Something went wrong when getting question amount. \(error)")
            return 0
        }
    }
    
    /* Compares the amount  */
    func updateQuestionsIfNeeded () {
        let amount = questionAmount()
        if amount < _minStoreLimit {
            /* Collecting the data asynchronously */
            APIManager.fetchQuestions(numberOfQuestions: _storeAmount, token: nil, completetionHandler: { responseFromAPI, error in
                /* Don't do anything if there is no response */
                if(responseFromAPI == nil){ return }
                /* Do the updating on the main thread, it will go bad otherwise */
                DispatchQueue.main.async { [unowned self] in
                    self.updateQuestions( (responseFromAPI?.questions)! )
                }
            })
        }
    }
    
    /* Stores an array of questions */
    private func updateQuestions(_ newQuestions : [Question]) {
        for i in 0..<newQuestions.count {
            let entity = NSEntityDescription.entity(forEntityName: "QuestionDB", in: _context)
            /* Tests that all data can be saved */
            if
                let correctAnswer = String(data: Data(base64Encoded: newQuestions[i].correctAnswer!)!, encoding: .utf8),
                let question = String(data: Data(base64Encoded: newQuestions[i].question!)!, encoding: .utf8),
                let category = String(data: Data(base64Encoded: newQuestions[i].category!)!, encoding: .utf8),
                let difficulty = String(data: Data(base64Encoded: newQuestions[i].difficulty!)!, encoding: .utf8),
                let type = String(data: Data(base64Encoded: newQuestions[i].type!)!, encoding: .utf8),
                let incorrectAnswer0 = String(data: Data(base64Encoded: newQuestions[i].incorrectAnswers![0])!, encoding: .utf8),
                let incorrectAnswer1 = String(data: Data(base64Encoded: newQuestions[i].incorrectAnswers![1])!, encoding: .utf8),
                let incorrectAnswer2 = String(data: Data(base64Encoded: newQuestions[i].incorrectAnswers![2])!, encoding: .utf8) {
                
                let newQuestion = NSManagedObject(entity: entity!, insertInto: _context)
                
                newQuestion.setValue(correctAnswer, forKey: "correctAnswer")
                newQuestion.setValue(incorrectAnswer0, forKey: "incorrectAnswer0")
                newQuestion.setValue(incorrectAnswer1, forKey: "incorrectAnswer1")
                newQuestion.setValue(incorrectAnswer2, forKey: "incorrectAnswer2")
                newQuestion.setValue(question, forKey: "questionText")
                newQuestion.setValue(category, forKey: "category")
                newQuestion.setValue(difficulty, forKey: "difficulty")
                newQuestion.setValue(type, forKey: "type")
                
                do { try _context.save() }
                catch { print("Something went wrong when storing question. \(error)") }
                
            }
        
        }
    }
    
    func getQuestion() -> Question? {
        if(self.questionAmount() == 0){
            return nil
        }
        
        let questionFetch  = NSFetchRequest<QuestionDB>(entityName: "QuestionDB")
        questionFetch.returnsObjectsAsFaults = false
        questionFetch.fetchLimit = 1
        do{
            let fetchedQuestion = try _context.fetch(questionFetch)[0]
            let incorrectAnswers : [String] = [
                fetchedQuestion.value(forKey: "incorrectAnswer0") as! String,
                fetchedQuestion.value(forKey: "incorrectAnswer1") as! String,
                fetchedQuestion.value(forKey: "incorrectAnswer2") as! String
            ]
            let theQuestion = Question (
                category: fetchedQuestion.value(forKey: "category") as! String,
                type: fetchedQuestion.value(forKey: "type") as! String,
                difficulty: fetchedQuestion.value(forKey: "difficulty") as! String,
                question: fetchedQuestion.value(forKey: "questionText") as! String,
                correctAnswer: fetchedQuestion.value(forKey: "correctAnswer") as! String,
                incorrectAnswers: incorrectAnswers)
            
            _context.delete(fetchedQuestion)
            try _context.save()
            updateQuestionsIfNeeded()
            return theQuestion;
        } catch {
            print("Something went wrong when popping question. \(error)")
            return nil
        }
    }
    
}
